﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour {
    public Animator animator;

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            if (EventSystem.current.currentSelectedGameObject.name == "Play Button")
            {
                LevelFade();
            }
        }  
    }

    public void LevelFade() {
        animator.SetTrigger("FadeOutTrigger");
    }

    public void FadeComplete() {
        Information.whichAdditive = "play";
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
