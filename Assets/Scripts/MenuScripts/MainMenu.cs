﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    public void SolveMazeButton() {
        Information.foundPosition = false;
        Information.whichAdditive = "solve";
        SceneManager.LoadScene("Map");
    }

    public void BackToMainMenu() {
        Information.foundPosition = false;
        Information.rows = 5;
        Information.columns = 5;
        Time.timeScale = 1f;
        Information.cameraSize = 1.8f;
        SceneManager.LoadScene("MenuScene");
    }
    public void QuitGame() {
        Application.Quit();
    }
}
