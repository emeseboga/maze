﻿using UnityEngine;

public class OptionsMenu : MonoBehaviour {

    public void SetDifficulty(int difficulty) {
        switch (difficulty) {
        case 0:
            Information.columns = 5;
            Information.rows = 5;
            Information.cameraSize = 1.8f;

            break;

        case 1:
            Information.columns = 7;
            Information.rows = 7;
            Information.cameraSize = 2f;

            break;

        case 2:
            Information.columns = 8;
            Information.rows = 8;
            Information.cameraSize = 2.3f;

            break;
        }
    }
}
