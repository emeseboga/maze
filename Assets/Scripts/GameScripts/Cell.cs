﻿using UnityEngine;

public class Cell {
    public bool visited = false;
    public GameObject northWall;
    public GameObject southWall;
    public GameObject eastWall;
    public GameObject westWall;
    public GameObject floor;
}
