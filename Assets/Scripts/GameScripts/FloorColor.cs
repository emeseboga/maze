﻿using UnityEngine;

public class FloorColor : MonoBehaviour {
    Color mouseOverColor = Color.gray;

    Color originalColor;

    MeshRenderer renderer;

    void Start() {
        renderer = GetComponent<MeshRenderer>();
        originalColor = renderer.material.color;
    }

    private void OnMouseOver() {
        if (Cursor.visible == true && Information.whichAdditive != "solve") {
            renderer.material.color = mouseOverColor;
        }
    }

    private void OnMouseExit() {
        if(Cursor.visible == true && Information.whichAdditive != "solve") {
            renderer.material.color = originalColor;
        }
    }

}
