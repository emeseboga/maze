﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GuessPosition : MonoBehaviour {
    public TextMeshProUGUI resultText;
    private MeshRenderer renderer;
    void Start() {
        renderer = GameObject.Find(Information.lastFloor).GetComponent<MeshRenderer>();
    }
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100)) {
                if(hit.transform.gameObject.name == Information.lastFloor) {
                    resultText.text = "Correct! You guessed the position right";
                } else if (hit.transform.gameObject.tag == "Floor")
                {
                    resultText.text = "Wrong! You did not guess your last position";
                } else {
                    return;
                }
                Cursor.visible = false;
                renderer.material.color = Color.green;
                Invoke("changeScene", 3);
            }
        }
    }

    public void changeScene() {
        Cursor.visible = true;
        SceneManager.LoadScene("EndScene");
    }
}
