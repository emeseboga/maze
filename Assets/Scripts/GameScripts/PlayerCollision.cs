﻿using UnityEngine;

public class PlayerCollision : MonoBehaviour {
    private void OnCollisionEnter(Collision collision) {
        if(collision.collider.tag == "Wall") {
            return;
        }
        Information.lastFloor = collision.collider.name;
    }

    private void OnCollisionStay(Collision collision) {
        if(collision.collider.tag == "Wall") {
            return;
        }
    }
}
