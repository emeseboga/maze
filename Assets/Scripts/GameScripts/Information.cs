﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Information {
    public static int rows = 5, columns = 5;
    public static float cameraSize = 1.8f;
    public static string whichAdditive = "solve";
    public static float time;
    public static bool foundPosition = false;
    public static Color floorColor;
    public static string lastFloor; 
}
