﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    private float moveSpeed = 0.8f;

    void Start() {
        int xPos = Random.Range(0, Information.columns);
        int zPos = Random.Range(0, Information.rows);
        GameObject floor = GameObject.Find("Floor " + zPos + "," + xPos);
        transform.Translate(floor.transform.position);
    }

    void FixedUpdate() {
        transform.Translate(moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime, 0f,moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime);
    }
}
