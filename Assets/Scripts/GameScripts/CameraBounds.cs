﻿using UnityEngine;

public class CameraBounds : MonoBehaviour {
    public Transform target;
    public Vector3 minBounds;
    public Vector3 maxBounds;

    private Camera camera;
    public float halfHeight;
    public float halfWidth;

    private void Start() {
        minBounds = new Vector3(-1, -0.5f, Information.rows - 1);
        maxBounds = new Vector3(Information.rows * 2 - 1, 0.5f, Information.rows - 1);

        camera = GetComponent<Camera>();
        camera.orthographicSize = Information.rows;
        halfHeight = Information.rows;
        halfWidth = halfHeight * Screen.width / Screen.height;
    }

    private void LateUpdate() {
        transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);

        float clampedX = Mathf.Clamp(transform.position.x, minBounds.x + halfWidth, maxBounds.x - halfWidth);
        float clampedZ = Mathf.Clamp(transform.position.z, minBounds.x + halfHeight, maxBounds.x - halfHeight);

        transform.position = new Vector3(clampedX, transform.position.y, clampedZ);
    }


}
