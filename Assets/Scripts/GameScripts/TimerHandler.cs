﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class TimerHandler : MonoBehaviour {
    private float timer = 16f;
    private TextMeshProUGUI timerText;

    private void Start() {
        timerText = GetComponent<TextMeshProUGUI>();
        timer = Information.time;
    }

    void Update() {
        timer -= Time.deltaTime;
        timerText.text = "Time Left: " + timer.ToString("f0");
        if (timer <= 0) {
            Cursor.visible = true;
            SceneManager.UnloadScene("MazeScene");
            SceneManager.LoadSceneAsync("GuessScene", LoadSceneMode.Additive);
        }
    }
}
