﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMaze : MonoBehaviour {
    private int mazeRows, mazeColumns;
    public GameObject wall;
    public GameObject floor;
    public float size = 2f;

    private Cell[,] mazeCells;
    public Cell GetCell(int x, int z) {
        try {
            return mazeCells[x, z];
        }
        catch (System.Exception e) {
            Debug.LogError("LoadMaze::GetCell() --- " + e);
            return null;
        }
    }

    void Start() {
        Cursor.visible = false;
        mazeRows = Information.rows;
        mazeColumns = Information.columns;
        InitializeMaze();
        HuntAndKillAlgorithm maze = new HuntAndKillAlgorithm(mazeCells);
        maze.CreateMaze();
        if (Information.whichAdditive == "play") {
            SceneManager.LoadSceneAsync("BeforeMazeScene", LoadSceneMode.Additive);
        } else {
            Cursor.visible = true;
            SceneManager.LoadSceneAsync("SolveMazeScene", LoadSceneMode.Additive);
        }
    }

    private void InitializeMaze() {

        mazeCells = new Cell[mazeRows, mazeColumns];

        for (int i = 0; i < mazeRows; i++) {
            for (int j = 0; j < mazeColumns; j++) {
                mazeCells[i, j] = new Cell();

                floor.GetComponent<FloorPositions>().Setup(i, j);
                mazeCells[i, j].floor = Instantiate(floor, new Vector3(i * size, -(size / 2f), j * size), Quaternion.identity) as GameObject;
                mazeCells[i, j].floor.name = "Floor " + j + "," + i;
                mazeCells[i, j].floor.transform.Rotate(Vector3.right, 90f);

                mazeCells[i, j].westWall = Instantiate(wall, new Vector3(i * size, 0, (j * size) - (size / 2f)), Quaternion.identity) as GameObject;
                mazeCells[i, j].westWall.name = "West Wall " + j + "," + i;

                mazeCells[i, j].eastWall = Instantiate(wall, new Vector3(i * size, 0, (j * size) + (size / 2f)), Quaternion.identity) as GameObject;
                mazeCells[i, j].eastWall.name = "East Wall " + j + "," + i;

                mazeCells[i, j].northWall = Instantiate(wall, new Vector3((i * size) - (size / 2f), 0, j * size), Quaternion.identity) as GameObject;
                mazeCells[i, j].northWall.name = "North Wall " + j + "," + i;
                mazeCells[i, j].northWall.transform.Rotate(Vector3.up * 90f);

                mazeCells[i, j].southWall = Instantiate(wall, new Vector3((i * size) + (size / 2f), 0, j * size), Quaternion.identity) as GameObject;
                mazeCells[i, j].southWall.name = "South Wall " + j + "," + i;
                mazeCells[i, j].southWall.transform.Rotate(Vector3.up * 90f);
            }
        }
    }
}
