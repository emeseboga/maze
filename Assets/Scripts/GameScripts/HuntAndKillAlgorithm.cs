using UnityEngine;

public class HuntAndKillAlgorithm {
    private Cell[,] cells;
    private int rows, columns;
    private int currentRow = 0;
    private int currentColumn = 0;

    private bool allCellsVisited = false;

    public HuntAndKillAlgorithm(Cell[,] cells) {
        this.cells = cells;
        rows = cells.GetLength(0);
        columns = cells.GetLength(1);
    }

    public void CreateMaze() {
        HuntAndKill();
    }

    public Cell[,] getMazeCells() {
        return cells;
    }

    private void KillPhase() {
        while (RouteIsAvaible(currentRow, currentColumn)) {
            int direction = Random.Range(1, 5);

            if (direction == 1 && CellIsAvailable(currentRow - 1, currentColumn)) {
                // North
                DestroyWallIfItExists(cells[currentRow, currentColumn].northWall);
                DestroyWallIfItExists(cells[currentRow - 1, currentColumn].southWall);
                currentRow--;
            } else if (direction == 2 && CellIsAvailable(currentRow + 1, currentColumn)) {
                // South
                DestroyWallIfItExists(cells[currentRow, currentColumn].southWall);
                DestroyWallIfItExists(cells[currentRow + 1, currentColumn].northWall);
                currentRow++;
            } else if (direction == 3 && CellIsAvailable(currentRow, currentColumn + 1)) {
                // East
                DestroyWallIfItExists(cells[currentRow, currentColumn].eastWall);
                DestroyWallIfItExists(cells[currentRow, currentColumn + 1].westWall);
                currentColumn++;
            } else if (direction == 4 && CellIsAvailable(currentRow, currentColumn - 1)) {
                // West
                DestroyWallIfItExists(cells[currentRow, currentColumn].westWall);
                DestroyWallIfItExists(cells[currentRow, currentColumn - 1].eastWall);
                currentColumn--;
            }

            cells[currentRow, currentColumn].visited = true;
        }
    }

    private void HuntPhase() {
        allCellsVisited = true; 

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (!cells[i, j].visited && CellHasVisitedNeighbour(i, j)) {
                    allCellsVisited = false; 
                    currentRow = i;
                    currentColumn = j;
                    DestroyNeighbourWall(currentRow, currentColumn);
                    cells[currentRow, currentColumn].visited = true;
                    return; 
                }
            }
        }
    }
    private void HuntAndKill() {
        cells[currentRow, currentColumn].visited = true;
        while (!allCellsVisited) {
            KillPhase(); 
            HuntPhase();
        }
    }
    private bool RouteIsAvaible(int row, int column) {
        int availableRoutes = 0;

        if (row > 0 && !cells[row - 1, column].visited) {
            availableRoutes++;
        }

        if (row < rows - 1 && !cells[row + 1, column].visited) {
            availableRoutes++;
        }

        if (column > 0 && !cells[row, column - 1].visited) {
            availableRoutes++;
        }

        if (column < columns - 1 && !cells[row, column + 1].visited) {
            availableRoutes++;
        }

        return availableRoutes > 0;
    }

    private bool CellIsAvailable(int row, int column) {
        if (row >= 0 && row < rows && column >= 0 && column < columns && !cells[row, column].visited) {
            return true;
        } else {
            return false;
        }
    }

    private void DestroyWallIfItExists(GameObject wall) {
        if (wall != null) {
            GameObject.Destroy(wall);
        }
    }

    private bool CellHasVisitedNeighbour(int row, int column) {
        int visitedCells = 0;

        // Look North
        if (row > 0 && cells[row - 1, column].visited) {
            visitedCells++;
        }

        // Look South
        if (row < (rows - 2) && cells[row + 1, column].visited) {
            visitedCells++;
        }

        // Look West
        if (column > 0 && cells[row, column - 1].visited) {
            visitedCells++;
        }

        // Look East
        if (column < (columns - 2) && cells[row, column + 1].visited) {
            visitedCells++;
        }

        return visitedCells > 0;
    }

    private void DestroyNeighbourWall(int row, int column) {
        bool wallDestroyed = false;

        while (!wallDestroyed) {
            int direction = Random.Range(1, 5);

            if (direction == 1 && row > 0 && cells[row - 1, column].visited) {
                DestroyWallIfItExists(cells[row, column].northWall);
                DestroyWallIfItExists(cells[row - 1, column].southWall);
                wallDestroyed = true;
            } else if (direction == 2 && row < (rows - 2) && cells[row + 1, column].visited) {
                DestroyWallIfItExists(cells[row, column].southWall);
                DestroyWallIfItExists(cells[row + 1, column].northWall);
                wallDestroyed = true;
            } else if (direction == 3 && column > 0 && cells[row, column - 1].visited) {
                DestroyWallIfItExists(cells[row, column].westWall);
                DestroyWallIfItExists(cells[row, column - 1].eastWall);
                wallDestroyed = true;
            } else if (direction == 4 && column < (columns - 2) && cells[row, column + 1].visited) {
                DestroyWallIfItExists(cells[row, column].eastWall);
                DestroyWallIfItExists(cells[row, column + 1].westWall);
                wallDestroyed = true;
            }
        }
    }

}
