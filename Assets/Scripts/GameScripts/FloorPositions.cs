﻿using UnityEngine;

public class FloorPositions : MonoBehaviour
{
    [SerializeField] private Transform targetPoint;
    public Transform TargetPoint { get { return targetPoint; } }

    public int x { get; private set; }
    public int y { get; private set; }

    public void Setup(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
