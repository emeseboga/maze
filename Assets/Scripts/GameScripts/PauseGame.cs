using UnityEngine;
using UnityEngine.EventSystems;

public class PauseGame : MonoBehaviour {

    private static bool GameIsPaused = false;
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
          if (GameIsPaused) {
              Resume();
          } else {
              Pause();
          }
        }
    }

    void Pause() {
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    void Resume() {
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
}
