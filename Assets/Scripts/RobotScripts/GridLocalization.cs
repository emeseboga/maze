using UnityEngine;
using UnityEngine.AI;

public class GridLocalization : MonoBehaviour { 

    private float[,] localization;
    private float[,] priors;
    private int size;
    private float eta;
    private float sum;
    private Color probabilityColor;
    private Color floorColor;
    private BoxCollider robot;

    void Awake() {
        robot = GetComponent<BoxCollider>();
    }

    void Start() {
        probabilityColor = Color.red;
        floorColor = GameObject.FindGameObjectWithTag("Floor").GetComponent<MeshRenderer>().material.color;
        Information.floorColor = floorColor;
        size = Information.rows;

        localization = new float[size, size];
        priors = new float[size, size];

        float value = 1.0f / (size * size);
        eta = 1.0f;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                priors[i, j] = value;
                localization[i, j] = value;
            }
        }
    }

    private float MotionModel(int row, int column, string direction) {
        float prob = 0.0f;
        
        switch (direction) {
            case "Right":
                if (column - 1 >= 0 && GameObject.Find("South Wall " + row.ToString() + "," + (column - 1).ToString()) == null ) {
                   prob = priors[row, column - 1] * 1.0f;
                }
                break;
            case "Left":
                if (column + 1 <= size - 1 && GameObject.Find("North Wall " + row.ToString() + "," + (column + 1).ToString()) == null ) {
                    prob = priors[row, column + 1] * 1.0f;
                }
                break;
            case "Up":
                if (row - 1 >= 0 && GameObject.Find("East Wall " + (row - 1).ToString() + "," + column.ToString()) == null) {
                    prob = priors[row - 1, column] * 1.0f;
                }
                break;
            case "Down":
                if (row + 1 <= size - 1  && GameObject.Find("West Wall " + (row + 1).ToString() + "," + column.ToString()) == null) {
                    prob = priors[row + 1, column] * 1.0f;
                }
                break;
        }
        return prob;
    }

    private float MeasurementModel(int row, int column, string direction) {
        float prob = 0.0f;
        RaycastHit hit;
        Vector3 startPosition = transform.position;
        startPosition += transform.forward * 0.5f;
        
        //If we have a wall right in front of us
        if (Physics.Raycast(startPosition, transform.forward, out hit, 2.0f)) {
            switch (direction) {
            case "Right":
                if (GameObject.Find("South Wall " + row.ToString() + "," + column.ToString()) != null ) {
                    prob = 1.0f;
                }
                break;
            case "Left":
                if (GameObject.Find("North Wall " + row.ToString() + "," + column.ToString()) != null ) {
                    prob = 1.0f;
                }
                break;
            case "Up":
                if (GameObject.Find("East Wall " + row.ToString() + "," + column.ToString()) != null) {
                    prob = 1.0f;
                }
                break;
            case "Down":
                if (GameObject.Find("West Wall " + row.ToString() + "," + column.ToString()) != null) {
                    prob = 1.0f;
                }
                break;
            }
        //If we don't see a wall
        } else {
            switch (direction) {
                case "Right":
                    if (GameObject.Find("South Wall " + row.ToString() + "," + column.ToString()) == null ) {
                        prob = 1.0f;
                    }
                    break;
                case "Left":
                    if (GameObject.Find("North Wall " + row.ToString() + "," + column.ToString()) == null ) {
                        prob = 1.0f;
                    }
                    break;
                case "Up":
                    if (GameObject.Find("East Wall " + row.ToString() + "," + column.ToString()) == null) {
                        prob = 1.0f;
                    }
                    break;
                case "Down":
                    if (GameObject.Find("West Wall " + row.ToString() + "," + column.ToString()) == null) {
                        prob = 1.0f;
                    }
                    break;
            }
        }
        return prob;
    }

    void OnCollisionEnter(Collision collision) {

        string direction = null;
        Vector3 localVelocity = (collision.transform.position - transform.position).normalized;
        float x = Mathf.Round(localVelocity.x * 10f) / 10f;
        float z = Mathf.Round(localVelocity.z * 10f) / 10f;
        if (x == 0.9f) {
            Debug.Log("Right");
            direction = "Right";
        } else if (x == -0.9f) {
            Debug.Log("Left");
            direction = "Left";
        } else if (z == 0.9f) {
            Debug.Log("Up");
            direction = "Up";
        } else if (z == -0.9f) {
            Debug.Log("Down");
            direction = "Down";
        }

        if (direction != null) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    float motion = MotionModel(i, j, direction);
                    float measurement = MeasurementModel(i, j, direction) * eta;
                    localization[i,j] = motion * measurement;
                }
            }
            sum = 0.0f;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    priors[i, j] = localization[i, j];
                    sum += localization[i, j];
                }
            }
            eta = 1.0f / sum;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    MeshRenderer renderer = GameObject.Find("Floor " + i.ToString() + "," + j.ToString()).GetComponent<MeshRenderer>();
                    if (localization[i, j] > 0.0f) {
                        //probabilityColor.a = localization[i, j];
                        renderer.material.color = probabilityColor;
                    } else {
                        renderer.material.color = floorColor;
                    }
                    if (localization[i, j] == 1.0f) {
                        Information.foundPosition = true;
                    }
                }
            }
        }
    }
}