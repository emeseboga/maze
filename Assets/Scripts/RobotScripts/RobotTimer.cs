using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using TMPro;
using UnityEngine.SceneManagement;

public class RobotTimer : MonoBehaviour {
    private float timer = 0f;
    private TextMeshProUGUI timerText;

    private void Start() {
        timerText = GameObject.FindWithTag("RobotTime").GetComponent<TextMeshProUGUI>();
    }

    void Update() {
        timer += Time.deltaTime;
        timerText.text = "Time: " + timer.ToString("f0");
        if (Information.foundPosition) {
            Information.time = timer;
            SceneManager.UnloadScene("BeforeMazeScene");
            GameObject.Destroy(GameObject.FindWithTag("Robot"));
            GameObject.Destroy(GameObject.FindWithTag("RobotSceneManager"));
            for (int i = 0; i < Information.rows; i++) {
                for (int j = 0; j < Information.rows; j++) {
                    MeshRenderer renderer = GameObject.Find("Floor " + i.ToString() + "," + j.ToString()).GetComponent<MeshRenderer>();
                    renderer.material.color = Information.floorColor;
                }
            }
            SceneManager.LoadSceneAsync("MazeScene", LoadSceneMode.Additive);
        }
    }
}