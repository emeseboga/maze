﻿using System.Collections.Generic;
using UnityEngine;

public class MoveToTile : MonoBehaviour
{
    private FloorPositions[] allFloors = null;
    private List<FloorPositions> visited = new List<FloorPositions>();
    private float timer = 3f;

    private PointBasedAI AI;
    private FloorPositions currentFloor;

    private void Awake() {
        AI = GetComponent<PointBasedAI>();
    }

    void Update()
    {
        if (timer > 0) {
            timer -= Time.deltaTime;
            return;
        }

        if (allFloors == null) {
            allFloors = FindObjectsOfType<FloorPositions>();
            currentFloor = allFloors[0];
            currentFloor = GetClosestFloor();
        }

        if (!AI.IsMoving()) {
            currentFloor = GetClosestFloor();
            //currentFloor.GetComponent<Renderer>().material.color = Color.red;
            visited.Add(currentFloor);

            AI.GoTo(currentFloor.transform.position);
        }
    }

    private FloorPositions GetClosestFloor() {
        float closestDist = 1000;
        var closestFloor = allFloors[0];

        foreach (var floor in allFloors) {
            if (visited.Contains(floor)) continue;

            var target = floor.TargetPoint.position;
            target.y = transform.position.y;

            if (!IsReachable(target)) continue;

            var dist = Vector3.Distance(gameObject.transform.position, target);

            if (dist < closestDist) {
                closestFloor = floor;
                closestDist = dist;
            }
        }

        if (!IsReachable(closestFloor.TargetPoint.position)) {
            visited.Clear();
            visited.Add(currentFloor);
            closestFloor = GetClosestFloor();
        }

        return closestFloor;
    }
    private bool IsReachable(Vector3 pos) {
        RaycastHit hit;
        
        if (Physics.Raycast(transform.position, (pos - transform.position), out hit, Vector3.Distance(transform.position, pos))) {
            if (hit.transform.gameObject.tag == "Wall" || hit.transform.gameObject.tag == "Floor") {
                return false;
            }  
        }

        return true;
    }
}
