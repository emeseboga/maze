﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointBasedAI : MonoBehaviour
{
    private bool moving = false;
    private bool turning = false;

    [SerializeField] private float movementSpeed = 1f;
    [SerializeField] private float rotationSpeed = 1f;

    [Space(10)]
    [Tooltip("How close the robot has to be to the target to be considered done. The smaller the value, the more accurate.")]
    [SerializeField] private float movementForgivenessDistance = 0.05f;
    [Tooltip("How close the robot has to be to the target to be considered done. The smaller the value, the more accurate.")]
    [SerializeField] private float rotationForgivenessAngle = 0.01f;

    public bool IsMoving() { return moving || turning; }

    private Vector3 targetPos;

    private void Start() {
        moving = false;
    }

    private void Update() {
        if (targetPos == null) return;

        if (turning) {
            Rotate();
            return;
        }

        if (moving) Movement();
    }

    public void GoTo(Vector3 targetPos) {
        this.targetPos = targetPos;
        this.targetPos.y = transform.position.y;

        turning = true;
    }

    private void Rotate() {
        var dir = (targetPos - transform.position).normalized;

        if (dir == Vector3.zero) {
            moving = true;
            turning = false;
            return;
        }

        var lookRot = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * rotationSpeed);
        
        if (Quaternion.Angle(transform.rotation, lookRot) <= rotationForgivenessAngle) {
            moving = true;
            turning = false;
        }
    }

    private void Movement() {
        if (Vector3.Distance(transform.position, targetPos) > movementForgivenessDistance) {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, movementSpeed * Time.deltaTime);
        } else {
            moving = false;
        }
    }

    
}
