using UnityEngine;
using UnityEngine.AI;

public class SpawnRobot : MonoBehaviour {

    public GameObject robot;
    public Camera camera;

    private void Start() {
        SetCameraSize();
        Spawn();
    }

    private void Spawn() {
        int xPos = Random.Range(0, Information.columns);
        int zPos = Random.Range(0, Information.rows);
        GameObject floor = GameObject.Find("Floor " + zPos + "," + xPos);

        Instantiate(robot, floor.transform.position, Quaternion.identity);
    }

    private void SetCameraSize() {

        Vector3 rotation = new Vector3(67f, 0.574f, 0f);
        Vector3 position = new Vector3(3.95f, 12.17f, -0.66f);

        if (Information.rows == 7) {
            rotation = new Vector3(67f, -0.1f, -0.1f);
            position = new Vector3(8.5f, 12.97f, 2f);
        } else if (Information.rows == 8) {
            rotation = new Vector3(67f, -0.2f, -0.1f);
            position = new Vector3(8.5f, 14.01f, 2.43f);
        }

        camera.transform.position = position;
        camera.transform.rotation = Quaternion.Euler(rotation);
        camera.orthographicSize = Information.rows + 2;
    }
}