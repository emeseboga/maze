using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class RightSensorDraw : MonoBehaviour {

    private LineRenderer lineRenderer;
    private float halfDistance = 0.5f;

    void Start() {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update() {
        RaycastHit hit;
        Vector3 startPosition = transform.position;

        startPosition += transform.forward * halfDistance;
        startPosition += transform.right * halfDistance;

        if (Physics.Raycast(startPosition, transform.forward, out hit, 2.0f)) {
            lineRenderer.SetPosition(0, startPosition);
            lineRenderer.SetPosition(1, hit.point);
        } else {
            lineRenderer.SetPosition(0, startPosition);
            lineRenderer.SetPosition(1, startPosition);
        }
    }

}